/**
 * @author Amir Sanni <amirsanni@gmail.com>
 */


window.addEventListener('load', ()=>{
    'use strict';


    new Vue({
        el: '#wunderRegister',


        data(){
            return {
                msg: '',
                currentView: 'nameView',
                ff: {
                    firstName: '',
                    lastName: '',
                    phone: '',
                    street: '',
                    houseNum: '',
                    zipCode: '',
                    city: '',
                    accountOwner: '',
                    iban: ''
                }                
            };
        },


        created(){
            //check if we have data in localStorage
            let data = localStorage.getItem('wunderData');

            if(data){
                //assign to our form fields
                let parsedData = JSON.parse(data);

                (this.ff = parsedData.ff);
                this.currentView = parsedData.currentView;
            }
        },


        methods: {
            back(){
                switch(this.currentView){
                    case 'addressView':
                        this.currentView = "nameView";
                        break;

                    case 'accountView':
                        this.currentView = 'addressView';
                        break;
                }

                //save current data in localStorage
                this.saveInLocalStorage();
            },


            next(){
                let validated = false;

                switch(this.currentView){
                    case 'nameView':
                        validated = this.validateNameView();
                        
                        if(validated){
                            this.currentView = 'addressView';
                            this.msg = '';
                        }

                        else{
                            this.msg = "All fields are required";
                        }
                        break;

                    case 'addressView':
                        validated = this.validateAddressView();
                        
                        if(validated){
                            this.currentView = 'accountView';
                            this.msg = '';
                        }

                        else{
                            this.msg = "All fields are required";
                        }
                        break;

                    case 'accountView':
                        validated = this.validateAccountView();
                        
                        if(validated){
                            this.save();
                        }

                        else{
                            this.msg = "All fields are required";
                        }
                        break;
                }

                //save current data in localStorage
                this.saveInLocalStorage();
            },


            saveInLocalStorage(){
                let data = {
                    currentView: this.currentView,
                    ff: this.ff
                };

                localStorage.setItem('wunderData', JSON.stringify(data));
            },


            validateNameView(){
                return (Boolean)(this.ff.firstName && this.ff.lastName && this.ff.phone);
            },


            validateAddressView(){
                return (Boolean)(this.ff.street && this.ff.houseNum && this.ff.zipCode && this.ff.city);
            },


            validateAccountView(){
                return (Boolean)(this.ff.accountOwner && this.ff.iban);
            },


            save(){
                /**
                 * Send data to server
                 * Remove data from localStorage if successfully saved
                 * Display success/failure msg to user
                 * Clear form data
                 */

                this.saveInLocalStorage();

                this.msg = "Saving your data. Please wait...";

                let fd = $.extend(true, {}, this.ff);//copy this.ff without reference
                fd.action = 'register';
                
                $.ajax("index.php", {
                    method: "POST",
                    data: fd
                }).done((rd)=>{
                    if(rd.status){
                        this.msg = `Registration successful. Your ID is <b>${rd.paymentDataId}</b>`;

                        localStorage.removeItem('wunderData');
                        
                        setTimeout(()=>{
                            this.msg = '';
                            this.currentView = 'nameView';
                            this.clearFormFields();
                        }, 10000);
                    }

                    else{
                        this.msg = rd.msg;
                    }
                }).fail(()=>{
                    this.msg = 'Registration failed. Please try again';
                });
            },


            clearFormFields(){
                for(let i in this.ff){
                    this.ff[i] = '';
                }
            }
        },


        computed: {
            showNameView(){
                return this.currentView == 'nameView';
            },


            showAddressView(){
                return this.currentView == 'addressView';
            },


            showAccountView(){
                return this.currentView == 'accountView';
            },


            showBackBtn(){
                return ['addressView', 'accountView'].includes(this.currentView);
            }
        }
    });
});