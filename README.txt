# Performance Optimisations
- Minify JavaScript
- Fine-tune the database schema




# Things that could be done better.
- On the front-end, each view/form should be created as separate components and manage independently.
- Validate the fields (Client and server).
- Transpile the JavaScript to ensure the new syntaxes used work on all browsers.
- Log and handle errors better on the server-side.
- Use Guzzle for CURL requests.
- Create a Base class that will expose instances to most classes (Output, HTTPRequest etc). Presenters will extend this class.
- Save database connection credentials in an env file