<?php
namespace Wunder;

use Wunder\Helpers\Output;
use Wunder\Presenters\Register;

require 'vendor/autoload.php';

class Index
{
    public function main()
    {
        $action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING);

        switch($action){
            case 'register':
                $this->register();
                break;

            default:
                Output::render('Templates/register.html');
                break;
        }
    }



    private function register()
    {
        $first_name = filter_input(INPUT_POST, 'firstName', FILTER_SANITIZE_STRING);
        $last_name = filter_input(INPUT_POST, 'lastName', FILTER_SANITIZE_STRING);
        $phone = filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_STRING);
        $street = filter_input(INPUT_POST, 'street', FILTER_SANITIZE_STRING);
        $house_number = filter_input(INPUT_POST, 'houseNum', FILTER_SANITIZE_STRING);
        $zip_code = filter_input(INPUT_POST, 'zipCode', FILTER_SANITIZE_STRING);
        $city = filter_input(INPUT_POST, 'city', FILTER_SANITIZE_STRING);
        $account_owner = filter_input(INPUT_POST, 'accountOwner', FILTER_SANITIZE_STRING);
        $iban = filter_input(INPUT_POST, 'iban', FILTER_SANITIZE_STRING);

        $regObj = new Register();

        $res = $regObj->save([
            'first_name'=>$first_name,
            'last_name'=>$last_name,
            'phone_number'=>$phone,
            'street'=>$street,
            'house_number'=>$house_number,
            'zip_code'=>$zip_code,
            'city'=>$city,
            'account_owner'=>$account_owner,
            'iban'=>$iban
        ]);

        Output::json($res);
    }
}



$i = new Index;

$i->main();