<?php
namespace Wunder\Presenters;

use Wunder\Models\Register as RegisterModel;

class Register
{
    protected $regModel;

    public function __construct()
    {
        $this->regModel = new RegisterModel();
    }


    public function save(array $data)
    {
        /**
         * Save data in DB.
         * Send data to wunder API endpoint and return paymentDataId to user on success
         */

        $user_id = $this->regModel->save($data);

        if($user_id){
            //Save and get payment data ID
            $payment_data_id = $this->getPaymentDataId([
                'user_id'=>$user_id,
                'iban'=>$data['iban'],
                'account_owner'=>$data['account_owner']
            ]);

            if($payment_data_id){
                //update the user info with the payment data
                $this->regModel->updateTable($user_id, ['payment_data_id'=>$payment_data_id]);

                $res = ['status'=>1, 'msg'=>'Success', 'paymentDataId'=>$payment_data_id];
            }

            else{
                $res = ['status'=>0, 'msg'=>'Unable to save payment data'];
            }
        }

        else{
            $res = ['status'=>0, 'msg'=>"We are unable to process your request at this time. Please try again."];
        }

        return $res;
    }


    public function getPaymentDataId(array $user_info)
    {
        try{
            $url = "https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data";
            
            $curl = curl_init();
            
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            
            curl_setopt($curl, CURLOPT_POST, TRUE);
            
            //json_encode the post_data if $json_encode is TRUE
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode([
                'customerId'=>$user_info['user_id'],
                'iban'=>$user_info['iban'],
                'owner'=>$user_info['account_owner']
            ]));
            
            //SSL
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, TRUE);
            
            $response = curl_exec($curl);
            
            curl_close($curl);
            
            return $response ? json_decode($response)->paymentDataId : FALSE;
        }

        catch(\Exception $e){
            return FALSE;
        }
    }
}