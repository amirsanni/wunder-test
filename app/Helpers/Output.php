<?php
namespace Wunder\Helpers;

class Output
{
    public static function render(string $view_path, array $data=[])
    {
        extract($data);

        ob_start();

        require_once "{$view_path}";

        echo ob_get_clean();
    }


    public static function json(array $data, int $status_code=200)
    {
        header('Content-Type: application/json');

        http_response_code($status_code);

        echo json_encode($data);
    }
}