<?php
namespace Wunder\Models;

use Wunder\Models\DB;

class Register extends DB
{
    protected $table_name = 'users';

    public function save(array $data)
    {
        return $this->insert($this->table_name, $data);
    }


    public function updateTable(int $user_id, array $data)
    {
        return $this->update($this->table_name, $user_id, $data);
    }
}