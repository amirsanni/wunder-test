<?php
namespace Wunder\Models;

class DB
{
    protected $db;

    public function __construct()
    {
        $this->connect();
    }

    private function connect()
    {
        try{
            $this->db = new \PDO('mysql:host=localhost; dbname=wunder; charset=utf8', 'root', '', [
                \PDO::ATTR_EMULATE_PREPARES => false, \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
            ]);
        }

        catch(\Exception $e){
            echo $e->getMessage();
        }
    }


    protected function insert(string $table_name, array $data)
    {
        try{
            $db_columns = implode(', ', array_keys($data));//columns to insert into
            $columns_data = array_values($data);//values to insert into each column
            $values_placeholder = implode(', ', array_fill(0, count($columns_data), "?"));
            
            $query = $this->db->prepare("INSERT INTO {$table_name} ({$db_columns}) VALUES ({$values_placeholder})");
            
            $inserted = $query->execute($columns_data);
                
            return $this->db->lastInsertId();
        }

        catch(PDOException $e){
            return FALSE;
        }
    }


    protected function update(string $table_name, int $user_id, array $data)
    {
        try{
            $db_columns = array_keys($data);//columns to update
            
            $set = array_map(function($col_name){
                return $col_name." = :{$col_name}";
            }, $db_columns);

            $set_columns = implode(', ', $set);
            
            $query = $this->db->prepare("UPDATE {$table_name} SET {$set_columns}");
            
            return $query->execute($data);
        }

        catch(PDOException $e){
            return FALSE;
        }
    }
}