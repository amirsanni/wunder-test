<?php

use PHPUnit\Framework\TestCase;
use Wunder\Presenters\Register;

class RegisterTest extends TestCase
{
    public static $reg;

    public static function setupBeforeClass():void
    {
        self::$reg = new Register();
    }


    /**
     * 
     */
    public function data()
    {
        return [
            [
                ['first_name'=>"Amir",
                'last_name'=>"Sanni",
                'phone_number'=>"07086201801",
                'street'=>"Amir Street",
                'house_number'=>"House 1",
                'zip_code'=>"100001",
                'city'=>"Lagos",
                'account_owner'=>"Amir Sanni",
                'iban'=>"0928264-NDF0928"]
            ], [
                ['first_name'=>"Foo",
                'last_name'=>"Bar",
                'phone_number'=>"07016201801",
                'street'=>"Bar Street",
                'house_number'=>"House 2",
                'zip_code'=>"100001",
                'city'=>"Lagos",
                'account_owner'=>"Foo Bar",
                'iban'=>"0928264-NDF0929"]
            ], [
                ['first_name'=>"John",
                'last_name'=>"Doe",
                'phone_number'=>"07036201801",
                'street'=>"Doe Street",
                'house_number'=>"House 3",
                'zip_code'=>"100001",
                'city'=>"Lagos",
                'account_owner'=>"John Doe",
                'iban'=>"0928264-NDF0930"]
            ]
        ];
    }
    

    /**
     * @dataProvider data
     */
    public function testReg(array $data)
    {
        $res = self::$reg->save($data);

        $this->assertIsArray($res);
        $this->assertIsInt($res['status']);
        $this->assertSame($res['status'], 1);
        $this->assertArrayHasKey('paymentDataId', $res);
        $this->assertIsString($res['paymentDataId']);
    }


    public function testWunderApi()
    {
        $res = self::$reg->getPaymentDataId([
            'user_id'=>1,
            'iban'=>"192866145687",
            'account_owner'=>"John Doe"
        ]);

        $this->assertIsString($res);
    }
}